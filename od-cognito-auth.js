import { PolymerElement, html } from '@polymer/polymer';
import { CognitoAuth } from 'amazon-cognito-auth-js';
import { CognitoUserPool, CognitoUserAttribute, CognitoUser } from 'amazon-cognito-identity-js';
import './od-cognito-user-details.js';

export class ODCognitoAuth extends PolymerElement {  
    static get is() { return 'od-cognito-auth' }
    static get template() {
        return html`
            <style>
                :host {
                    display: none;
                }
            </style>
        `;
    }
    static get properties() {
        return {
            region: String,

            appClientId: String,

            identityPoolId: String,

            appWebDomain: String,

            tokenScopesArray: String,

            resourceScope: String,

            redirectUriSignIn: String,

            redirectUriSignOut: String,

            userPoolId: String,

            authenticated: {
                type: Boolean,
                notify: true,
                observer: '_sendAuthDone'
            },

            _loginUrl: {
                type: String,
                computed: 'computeLogin( appClientId, appWebDomain, redirectUriSignIn )'
            },  
            
            _signUpUrl: {
                type: String,
                computed: 'computeSignUp( appClientId, appWebDomain, redirectUriSignIn )'
            },  

            _auth: {
                type: Object,
                value: () => ( {} )
            },

            _cognitoUser: Object,

            _userDetails: Object
        };
    }

    connectedCallback() {
        super.connectedCallback();
        window.addEventListener( "od-toggle-authenticate", () => this.toggleAuth( this._auth ) );
        window.addEventListener( "od-cognito-signup", () => this.signUp() );
        window.addEventListener( "od-request-user-details", ( e ) => this._passUserDetails( e ) );
        window.addEventListener( "od-update-user-email", ( e ) => this._updateUserEmail( e ) );
        window.addEventListener( "od-verify-user-email", ( e ) => this._verifyUserEmail( e ) );
        window.addEventListener( "od-resend-verify-code", ( e ) => this._resendVerify( e ) );
        this._auth = this._initCognitoSDK();
        this.signInRefresh( this._auth );
    }

    disconnectedCallback() {
        super.disconnectedCallback();
        window.removeEventListener( "od-toggle-authenticate", () => this.toggleAuth( this._auth ) );
        window.removeEventListener( "od-cognito-signup", () => this.signUp() );
        window.removeEventListener( "od-request-user-details", ( e ) => this._passUserDetails( e ) );
        window.removeEventListener( "od-update-user-email", ( e ) => this._updateUserEmail( e ) );
        window.removeEventListener( "od-verify-user-email", ( e ) => this._verifyUserEmail( e ) );
        window.removeEventListener( "od-resend-verify-code", ( e ) => this._resendVerify( e ) );
    }

    _initCognitoSDK() {
        if ( this.resourceScope !== null && this.resourceScope !== undefined && this.resourceScope !== '' ){
            this.tokenScopesArray = 'openid,email,aws.cognito.signin.user.admin'
        }
        if ( this.tokenScopesArray === null || this.tokenScopesArray === undefined ){
            this.tokenScopesArray = 'openid,email,aws.cognito.signin.user.admin'
        }
        var authData = {
            ClientId: this.appClientId,
            AppWebDomain: this.appWebDomain,
            TokenScopesArray: this.tokenScopesArray.split( ',' ),
            RedirectUriSignIn: this.redirectUriSignIn,
            RedirectUriSignOut: this.redirectUriSignOut,
            UserPoolId: this.userPoolId,
            identityPoolId: this.identityPoolId
        };

        var auth = new CognitoAuth( authData );
        auth.userhandler = {
            onSuccess: result => {
                this._getUserDetails();
            },
            onFailure: err => {
                if ( err !== '{"error":"invalid_grant"}' ) {
                    try { 
                        this.signInRefresh( auth );
                    } catch ( error ) {
                        this.authenticated = false;
                        console.log( 'error: could not refresh session: ', error )
                    }
                } else {
                    this.authenticated = false;
                }
            }
        };
        auth.useCodeGrantFlow();
        return auth;
    }

    signInRefresh( _auth ) {
        var curUrl = window.location.href;
        var hasCode = curUrl.includes( '?code=' );
        var session = _auth.getCachedSession();
        var refToken = '';
        var hasRefToken = false;
        if ( session ) {
            refToken = session.getRefreshToken().getToken();
        }                
        var hasRefToken = refToken !== '';
        if( !hasCode && !hasRefToken ) {
            this.authenticated = false;
            return;
        }
        if( hasCode ) {
            _auth.parseCognitoWebResponse( curUrl );
        }
        if ( hasRefToken ) {
            _auth.refreshSession( refToken );
            _auth.getSession();  
        }                
    }

    computeLogin( appClientId, appWebDomain, redirectUriSignIn ) {
        return 'https://' + appWebDomain + '/login?redirect_uri=' + redirectUriSignIn + '&response_type=code&client_id=' + appClientId;
    }

    computeSignUp( appClientId, appWebDomain, redirectUriSignIn ) {
        return 'https://' + appWebDomain + '/signup?redirect_uri=' + redirectUriSignIn + '&response_type=code&client_id=' + appClientId;
    }

    _getUserDetails() {
        var poolData = {
            UserPoolId : this.userPoolId, // Your user pool id here
            ClientId : this.appClientId // Your client id here
        };
        var userPool = new CognitoUserPool( poolData );
        this._cognitoUser = userPool.getCurrentUser();  

        if ( this._cognitoUser === null ) { 
            this.authenticated = false;
            return;
        }
        this._cognitoUser.getSession( ( err, session ) => {
            if ( err ) {
                this.authenticated = false;
                return;
            }
            this._userDetails = {};
            this._userDetails.accessToken = session.accessToken;
            // NOTE: getSession must be called to authenticate user before calling getUserAttributes
            this._cognitoUser.getUserAttributes( ( err, attributes ) => {
                if ( err ) {
                    this.authenticated = false;
                    return;
                } else {
                    this._userDetails.attributes = {};
                    for ( var i in attributes ) {
                        var attr = attributes[i];
                        var name = attr.getName();
                        var value = attr.getValue();
                        if ( name === 'sub' ) {
                            this._userDetails.subId = value;
                        } else {                                        
                            this._userDetails.attributes[name] = value;
                        }
                    }                                
                    this.authenticated = true;
                }
            });
        });
    }

    _sendAuthDone() {
        this.dispatchEvent( new CustomEvent( 'od-auth-done', { 
            composed: true, 
            bubbles: true,
            'detail': {
                'userDetails': this.userDetails
            } 
        } ) );
    }

    toggleAuth( _auth ) {
        if ( this.authenticated === true ) {
            this.authenticated = false;
            alert( 'You have been successfully logged out.' );
            _auth.signOut();                    
        } else {
            window.location.replace( this._loginUrl );
            return;
        }
    }

    signUp() {
        window.location.replace( this._signUpUrl );
    }

    _passUserDetails( e ) {
        if( this.authenticated !== true && this.authenticated !== false ) {
            return;
        }
        if( e.detail && e.detail.callback ) {
            e.detail.callback( this.authenticated, this._userDetails );
        }                
    }

    _updateUserEmail( e ) {
        if( !e.detail || !e.detail.newEmail || !e.detail.callback ) {
            return;
        }
        var newEmail = e.detail.newEmail;
        if ( this._cognitoUser != null ) {
            this._cognitoUser.getSession( ( err, session ) => {
                if ( err ) {
                    return;
                }
                var attributeList = [];
                var attribute = {
                    Name : 'email',
                    Value : newEmail
                };
                
                var attribute = new CognitoUserAttribute( attribute );
                attributeList.push( attribute );

                this._cognitoUser.updateAttributes( attributeList, ( err, result ) => {
                    if ( err ) {
                        return;
                    }
                    this._userDetails.attributes.email_verified = 'false';
                    e.detail.callback();                       
                });
            });
        }
    }

    _verifyUserEmail( e ) {
        if( !e.detail || !e.detail.verifyCode || !e.detail.callback ) {
            return;
        }
        var verifyCode = e.detail.verifyCode;
        if ( this._cognitoUser != null ) {
            this._cognitoUser.getSession( ( err, session ) => {
                if ( err ) {
                    return;
                }
                this._cognitoUser.verifyAttribute( 'email', verifyCode, {
                    onSuccess: ( result ) => {
                        this._userDetails.attributes.email_verified = 'true';
                        alert( 'Email Successfully Verified!' );
                        e.detail.callback();                                
                    },
                    onFailure: ( err ) => {
                        alert( err.message || JSON.stringify( err ) );
                    }
                });                                               
            });
        }
    }

    _resendVerify() {
        if ( this._cognitoUser != null ) {
            this._cognitoUser.getSession( ( err, session ) => {
                if ( err ) {
                    return;
                }
                this._cognitoUser.getAttributeVerificationCode( 'email', {
                    onSuccess: function (result) {
                        alert( 'Confirmation Code has been re-sent!' );
                    },
                    onFailure: function(err) {
                        alert( err.message || JSON.stringify( err ) );
                    },
                    inputVerificationCode: null
                });                                               
            });
        }                
    }     
}
customElements.define( ODCognitoAuth.is, ODCognitoAuth );