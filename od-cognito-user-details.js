import { PolymerElement, html } from '@polymer/polymer';

class ODCognitoUserDetails extends PolymerElement {
    static get is() { return 'od-cognito-user-details' }
    static get template() {
        return html`
            <style>
                :host {
                    display: none;
                }
            </style>
        `;
    }
    static get properties() {
        return {
            authenticated: {
                type: Boolean,
                notify: true
            },

            userDetails: {
                type: Object,
                notify: true
            },

            emailVerified: {
                type: Boolean,
                notify: true,
                computed: '_setEmailVerified( userDetails.attributes.email_verified )'
            }

        };
    }

    connectedCallback() {
        super.connectedCallback();
        window.addEventListener( "od-auth-done", ( e ) => this.getUserDetails( e ) );
        this.getUserDetails();
    }

    disconnectedCallback() {
        super.disconnectedCallback();
        window.removeEventListener( "od-auth-done", ( e ) => this.getUserDetails( e ) );
    }

    getUserDetails() {         
        this.dispatchEvent( new CustomEvent( 'od-request-user-details', 
            { 
                composed: true, 
                bubbles: true,
                'detail': {
                    'callback': ( authenticated, userDetails ) => {
                        this.userDetails = userDetails;
                        this.authenticated = authenticated;
                    }
                } 
            } 
        ) );
    }

    _setEmailVerified( verified ) {
        return verified;
    }

    updateEmail( newEmail, verifyRoute ) {
        if( !this.authenticated || !newEmail || !verifyRoute ) {
            return;
        }
        if( newEmail === this.userDetails.attributes.email ) {
            return;
        }
        this.dispatchEvent( new CustomEvent( 'od-update-user-email', 
            { 
                composed: true, 
                bubbles: true,
                'detail': {
                    'newEmail': newEmail,
                    'callback': () => {
                        this.getUserDetails();
                        var event = new CustomEvent( 'od-route-change', { bubbles: true, composed: true, 'detail': { 'route': verifyRoute } } );
                        this.dispatchEvent( event );
                    }
                }
            }
        ) );
    }

    verifyEmail( verifyCode, redirectRoute ) {
        if( !this.authenticated || !verifyCode || !redirectRoute ) {
            return;
        }
        this.dispatchEvent( new CustomEvent( 'od-verify-user-email', 
            { 
                composed: true, 
                bubbles: true,
                'detail': {
                    'verifyCode': verifyCode,
                    'callback': () => {
                        window.location.replace( redirectRoute );
                    }
                } 
            } 
        ) );
    }

    resendVerify() {
        if( !this.authenticated ) {
            return;
        }
        this.dispatchEvent( new CustomEvent( 'od-resend-verify-code', { composed: true, bubbles: true } ) );              
    }
}
customElements.define( ODCognitoUserDetails.is, ODCognitoUserDetails );